from app import app
from flask import jsonify
import socket

@app.route('/', methods=['GET'])
def home():
    return "<h1>Hola, Soy Michael!</h1>"

@app.route('/greetings', methods=['GET'])
def greetings():
    return jsonify(
        message="Hello World from "+ socket.gethostname()
    )

@app.route('/square', methods=['GET'])
def square():
    return jsonify(
        message="number: X, square: Y, donde Y es el cuadrado de X. Se espera un response con el cuadrado"
        )