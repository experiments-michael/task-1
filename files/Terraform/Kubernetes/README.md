## Providers

| Name | Version |
|------|---------|
| kubernetes | ~> 1.13.2 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| gcp\_project | Proyecto | `string` | n/a | yes |
| gke\_password | Contraseña para ingresar al Kubernetes | `string` | n/a | yes |

## Outputs

No output.