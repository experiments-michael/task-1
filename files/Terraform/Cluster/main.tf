terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 3.39.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "~> 1.3.0"
    }
  }
}

resource "google_container_cluster" "main" {
  name               = var.gke_name
  location           = var.gke_zone
  initial_node_count = 1
  min_master_version = "1.16.13-gke.1"

  master_auth {
    username = var.gke_username
    password = var.gke_password

    client_certificate_config {
      issue_client_certificate = false
    }
  }
}

module "helm_nginx" {
  source = "./modules/helm/nginx"
  depends_on = [google_container_cluster.main]
}