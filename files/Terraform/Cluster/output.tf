output "gke_host" {
  value = google_container_cluster.main.endpoint
  description = "Endpoint del Cluster"
}

output "gke_username" {
  value = var.gke_username
  description = "Usuario del Cluster"
}