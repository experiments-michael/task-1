data "terraform_remote_state" "cluster" {
  backend = "local"

  config = {
    path = "../Cluster/terraform.tfstate"
  }
}

provider "kubernetes" {
    load_config_file       = false
    host = data.terraform_remote_state.cluster.outputs.gke_host
    username = data.terraform_remote_state.cluster.outputs.gke_username
    password = var.gke_password
    insecure = true
}