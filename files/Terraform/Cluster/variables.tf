variable "gcp_project" {
  type = string
  description = "Proyecto"
}
variable "gcp_region" {
  type = string
  description = "Región"
  default = "us-east1"
}
variable "gcp_credentials" {
  type = string
  description = "Ruta de archivo de credenciales para la conexión a Google Cloud"
}

variable "gke_name" {
  type = string
  description = "Nombre del Kubernetes"
  default = "kubernetes-michael"
}
variable "gke_zone" {
  type = string
  description = "Zona donde se ubicará el Kubernetes"
  default = "us-east1-b"
}

variable "gke_username" {
  type = string
  description = "Usuario para ingresar al Kubernetes"
  default = "admin"
}
variable "gke_password" {
  type = string
  description = "Contraseña para ingresar al Kubernetes"
}